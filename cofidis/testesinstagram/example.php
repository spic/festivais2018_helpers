<?php
/*****************************************************************
Created :  2017-02-08
Author : Mr. Khwanchai Kaewyos (LookHin)
E-mail : khwanchai@gmail.com
Website : https://www.unzeen.com
Facebook : https://www.facebook.com/LookHin
Source Code On Github : https://github.com/LookHin/instagram-photo-video-upload-api
Rewrite Code From : https://github.com/mgp25/Instagram-API
*****************************************************************/

include_once("instagram-photo-video-upload-api.class.php");

// Upload Photo
// $obj = new InstagramUpload();
// $obj->Login("YOUR_IG_USERNAME", "YOUR_IG_PASSWORD");
// $obj->UploadPhoto("square-image.jpg", "Test Upload Photo From PHP");

// Upload Video
$obj = new InstagramUpload();
$obj->Login("jgop7", "Ab281196");
$obj->UploadVideo("output_loop.mov", "square-thumb.jpg", "Test Upload Video From PHP");

?>