<?php

class globalstuff
{
    public $flag_error = "error";
    public $flag_ok = "ok";

    public $time_zone = 'Europe/Lisbon';
    private $folder_erros = "error_folder";

    private $desenvolvimento_flag;
    public $pathgeral;
    public $servidor;
    public $basefolders;
    public $nome_projeto;

    public $email_developer_array;
    public $from_email_php;
    
    private $session_name;
    private $secure_session;
    
    private $flag_https;

    function __construct(){
        // CONSTRUÇÃO DE CAMINHOS
        // caminho para angularjs tag "base"
        $url_base = $_SERVER['PHP_SELF'];
        $url_base_array = explode('/',$url_base);
        $this->basefolders = "";
        for ($i=0; $i < count($url_base_array)-1; $i++) { 
            $this->basefolders .= $url_base_array[$i]."/";
        } 
        
        
        // verificar https
        $https_string = "http://";
        $this->secure_session = false;
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $https_string = "https://";
            $this->secure_session = true;
        }
        // caminho para servidor
        $this->servidor = $https_string.$_SERVER['HTTP_HOST']."/";
        
        // caminho para a pasta do ficheiro
        $this->pathgeral = $https_string.$_SERVER['HTTP_HOST'].$this->basefolders;
        
        include "config/global.config.php";
        
        
        if($this->flag_https == 1 && !isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'on'){
            header("Location: https://".$_SERVER['HTTP_HOST']);
            exit();
        }
    }

    /**
     * Retorna o timestamp
     *
     * @return void
     */
    function return_timestamp(){
        date_default_timezone_set($this->time_zone);
        return date("Y-m-d H:i:s");
    }

    function return_date(){
        date_default_timezone_set($this->time_zone);
        return date("Y-m-d");
    }


    function isJson($string) {
        json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE);
    }
    
    /**
     * Dump do Array para String
     *
     * @param [type] $array
     * @param string $tabs
     * @return void
     */
    function arraydump_custom($array,$tabs = ""){
        $tab_reference = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\t";
        $string_return = "";
        $string_return .= "array(".count($array)."){";
        $string_return .="<br />\n";
        
        foreach ($array as $key => $value) {
            if(is_array($value) || is_object($value)){
                $string_return .= $tab_reference.$tabs."['".$key."']=>".$this->arraydump_custom($value,$tab_reference.$tabs);
            }
            else{
                $string_return .= $tab_reference.$tabs."['".$key."']=> ".$value;
                $string_return .="<br />\n";
            }
        }
        $string_return .= $tabs."}";
        $string_return .="<br />\n";
        
        return $string_return;
    }
    
    /**
     * error report
     *
     * @param [type] $error_string
     * @return void
     */
    function error_report($error_string){
        $flag = $this->desenvolvimento_flag;
        $folder = $this->folder_erros;

        if(is_array($error_string) || is_object($error_string)){
            $error_string = $this->arraydump_custom($error_string);
        }

        if($flag == 2){
            echo "############ Error Report:<br />";
            echo $error_string;
            echo "<br />";
        }
        else{ 
            if (!is_dir($folder)) {
                mkdir($folder,0777);
            }
            $string_identifier = $this->return_timestamp().":<br />\n";
            $string_identifier .= $_SERVER['HTTP_USER_AGENT']."<br />\n";
            $string_identifier .= "IP: ".$_SERVER['REMOTE_ADDR']."<br />\n";
            $string_identifier .= "URL: ".$_SERVER['REQUEST_URI']."<br />\n";
            $string_identifier .= "URL QUERY: ".$_SERVER['QUERY_STRING']."<br />\n";
            $string_identifier .= "FILE: ".$_SERVER['SCRIPT_FILENAME']."<br />\n";
            $string_identifier .= "ERROR DESCRIPTION:"."<br />\n<br />\n";
            
            if($flag <= 1){
                $string_identifier2 = $string_identifier . str_replace("&nbsp;", "", strip_tags($error_string));
                
                $fh = fopen($folder.'/'.basename($_SERVER["SCRIPT_FILENAME"])."_error_log.txt", (file_exists($folder.'/'.basename($_SERVER["SCRIPT_FILENAME"])."_error_log.txt")) ? 'a' : 'w');
                fwrite($fh, "######################################\n");	
                fwrite($fh, strip_tags($string_identifier2)."\n\n");	
				ob_start();
				$data = ob_get_clean();
				fclose($fh);
            }
            if($flag == 0){
                $headers  = "From: ".$this->nome_projeto." <".$this->from_email_php."> \n";
                $headers .= "MIME-Version: 1.0 \n";
                $headers .= "X-Priority: 1 ";
                $headers .= "X-MSmail-Priority: High \n";
                $headers .= "X-Mailer: PHP/" . phpversion()."\n";
                $headers .= "Content-type: text/html; charset=iso-8859-1 \n";

                $string_identifier .= $error_string;
                
                foreach ($this->email_developer_array as $key => $value) {
                    mail($value,"Erro ".$this->nome_projeto,$string_identifier,$headers);
                }
                
            }

        }
		
    } 
    
    /**
     * Return string friendly
     *
     * @param [type] $phrase
     * @param [type] $maxLength
     * @return void
     */
    function generateFriendlyName($phrase, $maxLength)
    {
        $result = preg_replace('/ã/', 'a', $phrase);
        $result = preg_replace('/Ã/', 'a', $result);
        $result = preg_replace('/á/', 'a', $result);
        $result = preg_replace('/Á/', 'a', $result);
        $result = preg_replace('/à/', 'a', $result);
        $result = preg_replace('/À/', 'a', $result);
        $result = preg_replace('/Â/', 'a', $result);
        $result = preg_replace('/â/', 'a', $result);

        $result = preg_replace('/Ç/', 'c', $result);
        $result = preg_replace('/ç/', 'c', $result);

        $result = preg_replace('/õ/', 'o', $result);
        $result = preg_replace('/Õ/', 'o', $result);
        $result = preg_replace('/ó/','o',$result);
        $result = preg_replace('/Ó/','o',$result);

        $result = preg_replace('/é/','e',$result);
        $result = preg_replace('/É/','e',$result);
        $result = preg_replace('/ê/','e',$result);
        $result = preg_replace('/Ê/','e',$result);

        $result = preg_replace('/í/','i',$result);
        $result = preg_replace('/Í/','i',$result);		

        $result = preg_replace('/ú/','u',$result);
        $result = preg_replace('/Ú/','u',$result);
        
        $result = preg_replace('/ñ/','n',$result);

        $result = strtolower($result);

        $result = preg_replace("/[^a-z0-9\s-]/", "", $result);
        $result = trim(preg_replace("/[\s-]+/", " ", $result));
        $result = trim(substr($result, 0, $maxLength));
        $result = preg_replace("/\s/", "-", $result);

        return $result;
    }
    
    /**
     * Divide um texto ao meio
     * @param type $text
     * @return array
     */
    function divide_text_middle($text){
        $array_text = explode(' ',$text);
        $text1 = "";
        $text2 = "";
        for ($i=0; $i < round(count($array_text)/2); $i++) { 
                $text1 .= $array_text[$i]." ";
        }
        for ($i=round(count($array_text)/2); $i < count($array_text); $i++) { 
                $text2 .= $array_text[$i]." ";
        }
        return array($text1, $text2);
    }
    
//    SESSION
    function sec_session_start() {
        $session_name = $this->session_name;   // Estabeleça um nome personalizado para a sessão
        $secure = $this->secure_session;
        // Isso impede que o JavaScript possa acessar a identificação da sessão.
        $httponly = true;
        // Assim você força a sessão a usar apenas cookies. 
       if (ini_set('session.use_only_cookies', 1) === FALSE) {
            exit();
        }
        // Obtém params de cookies atualizados.
        $cookieParams = session_get_cookie_params();
        session_set_cookie_params($cookieParams["lifetime"],
            $cookieParams["path"], 
            $cookieParams["domain"], 
            $secure,
            $httponly);
        // Estabelece o nome fornecido acima como o nome da sessão.
        session_name($session_name);
        session_start();            // Inicia a sessão PHP 
        session_regenerate_id();    // Recupera a sessão e deleta a anterior. 
    }
    
    function sec_session_destroy(){
        $this->sec_session_start();
 
        // Desfaz todos os valores da sessão  
        $_SESSION = array();

        // obtém os parâmetros da sessão 
        $params = session_get_cookie_params();

        // Deleta o cookie em uso. 
        setcookie(session_name(),
                '', time() - 42000, 
                $params["path"], 
                $params["domain"], 
                $params["secure"], 
                $params["httponly"]);

        // Destrói a sessão 
        session_destroy();
    }
    
    function generate_pass($total = 8)
    {
        $new_pass = '';
        $arrayNumbers = array(48,57);
        $arrayMin = array(65,90);
        $arrayCaps = array(97,122);
        $arrayInterval = array($arrayNumbers,$arrayMin,$arrayCaps);
        for ($i=0; $i < $total; $i++) {
                $codeLength = rand(0,2); 
                $code = rand( $arrayInterval[$codeLength][0] , $arrayInterval[$codeLength][1] );
                $char = chr($code);	
                $new_pass .= $char; 
        }
        return $new_pass;
    }
}