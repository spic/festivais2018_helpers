<?php
spl_autoload_register(function ($class) {
    include $class . '.class.php';
});
class festivais extends globalstuff{

    function __construct() {
        parent::__construct();
        
    }

    function error_mail($name, $error){
        if (!is_dir("errors")) {
            mkdir("errors",0777);
        }

        $folder = "errors/".$this->generateFriendlyName($name,200);
        if (!is_dir($folder)) {
            mkdir($folder,0777);
        }

        if($this->isJson($error)){
            $error = json_decode($error);
        }

        if(is_array($error) || is_object($error)){
            $error = $this->arraydump_custom($error);
        }

        $string_identifier = $this->return_timestamp().":<br />\n";
        $string_identifier .= "IP: ".$_SERVER['REMOTE_ADDR']."<br />\n";
        $string_identifier .= "ERROR DESCRIPTION:"."<br />\n<br />\n";
        $string_identifier2 = $string_identifier . str_replace("&nbsp;", "", strip_tags($error));
                
        $fh = fopen($folder.'/error_log.txt', (file_exists($folder."/error_log.txt")) ? 'a' : 'w');
        fwrite($fh, "######################################\n");	
        fwrite($fh, strip_tags($string_identifier2)."\n\n");	
        ob_start();
        $data = ob_get_clean();
        fclose($fh);

        $headers  = "From: ".$this->nome_projeto." <".$this->from_email_php."> \n";
        $headers .= "MIME-Version: 1.0 \n";
        $headers .= "X-Priority: 1 ";
        $headers .= "X-MSmail-Priority: High \n";
        $headers .= "X-Mailer: PHP/" . phpversion()."\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1 \n";

        $string_identifier .= $error;
        
        
        foreach ($this->email_developer_array as $key => $value) {
            mail($value,"Erro ".$this->nome_projeto." - ".$name,$string_identifier,$headers);
        }
    }
}
?>