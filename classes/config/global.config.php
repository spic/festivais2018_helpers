<?php
//0 - produção (segue o erro por email e escreve no ficheiro)
//1 - desenvolvimento (escrita em ficheiro)
//2 - desenvolvimentos (escrita no ecrã)
$this->desenvolvimento_flag = 0;

//nome do prejecto
$this->nome_projeto = "Festivais2018";

$this->email_developer_array = array("joao@spic.pt","celso@spic.pt","joaopereira@spic.pt");
$this->from_email_php = "noreply@spic.pt";

$this->session_name = "festivais2018";

$ip = $_SERVER["REMOTE_ADDR"];
if (strpos($ip, '192.168') !== false || $ip == "127.0.0.1" || $ip == "::1") {
    $this->flag_https = 0;
}
else{
    $this->flag_https = 0;
}
