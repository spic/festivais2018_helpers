<?php
spl_autoload_register(function ($class) {
    include $class . '.class.php';
});

/**
 * Description of img
 *
 * @author joaopalhinha
 */
class img extends file{
    
    private $imgObject = null;
    private $imgType = 0;
    private $width_o = 0;
    private $height_o = 0;
    private $width_act = 0;
    private $height_act = 0;
    public $name_saved = "";
    function __construct($imgname = null) {
        parent::__construct();
        
        if($imgname){
            $this->imgObject = $this->createImgObject($imgname);
        }
        
        
    }
    /**
     * Retorna o tipo da imagem
     * 1 - gif
     * 2 - jpg
     * 3 - png
     * @param String $img
     * @return int
     */
    function returnImgType($img){
        $tipo = exif_imagetype($img);
        if($tipo != 1 && $tipo != 2 && $tipo != 3)
            $tipo = false;
        return $tipo;
    }
    
    function createImgObject($img){
        $this->imgType = $this->returnImgType($img);
        list($this->width_o, $this->height_o) = getimagesize($img);
        list($this->width_act, $this->height_act) = getimagesize($img);
            
        switch ($this->imgType) {
            case 1:{
                $image = imagecreatefromgif($img);
                break;
            }
            case 2:{
                $image = imagecreatefromjpeg($img);
                break;
            }
            case 3:{
                $image = imagecreatefrompng($img);
                break;
            }
            default :{
                $image = false;
                break;
            }
        }
        return $image;
    }
    
    
    function cropImg($x,$y,$width,$height){
        $image2 = false;
        if($this->imgObject){
            if(function_exists('imagecrop')){
                $image2 = imagecrop($this->imgObject, ['x' => $x, 'y' => $y, 'width' => $width, 'height' => $height]);
            }
            else{
                $image2 = imagecreatetruecolor($width, $height);
                if($this->imgType == 3){
                    imagealphablending($image2, false);
                    imagesavealpha($image2, true);
                    $transparent = imagecolorallocatealpha($image2, 255, 255, 255, 127);
                    imagefilledrectangle($image2, 0, 0, $width, $height, $transparent); 
                }
                imagecopyresampled($image2, $this->imgObject, 0, 0, $x, $y, $width, $height, $width, $height);
            }
        }
        $this->width_act = $width;
        $this->height_act = $height;
        $this->imgObject = $image2;
        
    }
    
    function resizeImg($width,$height){
        
        if($width > $this->width_o){
            $width = $this->width_o;
        }
        if($height > $this->height_o){
            $height = $this->height_o;
        }
        $ratio_orig = $this->width_o/$this->height_o;
        if ($width/$height > $ratio_orig) 
        {
           $new_height = $width/$ratio_orig;
           $new_width = $width;
        } 
        else 
        {
           $new_width = $height*$ratio_orig;
           $new_height = $height;
        }
        $image2 = imagecreatetruecolor($new_width, $new_height);
        $this->width_act = $new_width;
        $this->height_act = $new_height;
        if($this->imgType == 3){
            imagealphablending($image2, false);
            imagesavealpha($image2, true);
            $transparent = imagecolorallocatealpha($image2, 255, 255, 255, 127);
            imagefilledrectangle($image2, 0, 0, $new_width, $new_height, $transparent); 
        }
        imagecopyresampled($image2, $this->imgObject, 0, 0, 0, 0, $new_width, $new_height, $this->width_o, $this->height_o);
        $this->imgObject = $image2;
    }
    
    function saveImg($nome_final,$folder,$nome_final_url = null,$qualityjpg = null, $qualitypng = null){
        $flag_save = false;
        if(!$nome_final_url)
            $nome_final_url = $this->generateFileName($nome_final);
        
        switch ($this->imgType) {
            case 1:{
                if($qualityjpg){
                    $flag_save = imagegif($this->imgObject,$folder.$nome_final_url,$qualityjpg);
                }
                else{
                    $flag_save = imagegif($this->imgObject,$folder.$nome_final_url);
                }
                
                break;
            }
            case 2:{
                if($qualityjpg){
                    $flag_save = imagejpeg($this->imgObject,$folder.$nome_final_url,$qualityjpg);
                }
                else{
                    $flag_save = imagejpeg($this->imgObject,$folder.$nome_final_url);
                }
                
                break;
            }
            case 3:{
                if($qualitypng){
                    $flag_save = imagepng($this->imgObject,$folder.$nome_final_url,$qualitypng);
                }
                else{
                    $flag_save = imagepng($this->imgObject,$folder.$nome_final_url);
                }
                
                break;
            }
        }
        imagedestroy($this->imgObject);
        if($flag_save)
            $this->name_saved = $nome_final_url;
        return $flag_save;
    }
    
    function uploadImageCrop($file,$folder,$escala,$width_final, $height_final, $x_corte_escala, $y_corte_escala,$width_escala,$height_escala){
        $return = false;
        if($imageFile = $this->uploadFile($file, $folder,array('jpg','jpeg','png','gif'))){
            $this->imgObject = $this->createImgObject($folder.$imageFile);
            if($this->imgObject){
                $this->resizeImg($width_escala*$escala, $height_escala*$escala);
                $this->cropImg($x_corte_escala*$escala*(-1), $y_corte_escala*$escala*(-1), $width_final, $height_final);
                $this->saveImg($imageFile,$folder);
            }
        }
    }
    
    function uploadImageCenterCrop($file,$folder,$width_final, $height_final, $nome_final = null, $qualityjpg = null, $qualitypng = null){
        $return = false;
        if($imageFile = $this->uploadFile($file, $folder,array('jpg','jpeg','png','gif'))){
            $this->imgObject = $this->createImgObject($folder.$imageFile);
            if($this->imgObject){
                if($width_final > 0 && $height_final > 0){
                    $this->resizeImg($width_final, $height_final);
                    $this->cropImg(round(($width_final-$this->width_act)/2)*(-1), round(($height_final-$this->height_act)/2)*(-1), $width_final, $height_final);
                }
                
                
                if ($nome_final) {
                    $return = $this->saveImg($imageFile, $folder, $nome_final,$qualityjpg,$qualitypng);
                } else {
                    $return = $this->saveImg($imageFile, $folder,null,$qualityjpg,$qualitypng);
                }
            }
        }
        return $return;
    }
    
    
}
