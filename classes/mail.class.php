<?php
spl_autoload_register(function ($class) {
    include $class . '.class.php';
});

/**
 * Description of mail
 *
 * @author joaopalhinha
 */
class mail extends globalstuff{
    
    private $url_mail;
    private $smtp_email;
    private $smtp_email_pass;
    private $organization_email;
    private $nome_from_email;
    private $server_email;
    private $ip_server_email;
    private $server_port;
    private $server_encript;
    
    function __construct() {
        include 'config/mail.config.php';
    }
    
    
    function SMTPmail($to_array, $subject, $body, $from = null, $name_from = null, $replyto = null, $password_smtp = null){
        require_once 'helpers/PHPMailer/PHPMailerAutoload.php';
        if(!$from)
        {
            $from = $this->smtp_email;
        }
        if(!$name_from)
        {
            $name_from = $this->nome_from_email;
        }
        if(!$replyto)
        {
            $replyto = $this->smtp_email;
        }
        if(!$password_smtp)
        {
            $password_smtp = $this->smtp_email_pass;
        }
        $mail = new PHPMailer;
            
        $mail->isSMTP();    
                                       // Set mailer to use SMTP
        $mail->SMTPAuth = true;    
        $mail->XMailer = ' ';
        $mail->Host = $this->server_email;  // Specify main and backup SMTP servers
                              // SMTP password

        $mail->Username = $from;                 // SMTP username
        $mail->Password = $password_smtp;                           // SMTP password

        $mail->SMTPSecure = $this->server_encript;                            // Enable TLS encryption, `ssl` also accepted


        $mail->Port = $this->server_port;                                    // TCP port to connect to

        $mail->setFrom($from, utf8_decode($name_from) );
        $mail->AddReplyTo($replyto);


        foreach($to_array as $key=>$value){
            $mail->addAddress($value);     // Add a recipient    
        }
        // $mail->addAddress($to);     // Add a recipient
        $mail->isHTML(true);

        $mail->Subject = $subject;
        $mail->Body    = $body;
        $mail->AltBody = "Por favor use um cliente de email com suporte para html.";

        $mail->addCustomHeader("X-Sender","<".$from.">" );
        $mail->addCustomHeader("X-Sender-IP",$this->ip_server_email);     
        $mail->addCustomHeader("Organization", $this->organization_email);     
        $mail->addCustomHeader("X-Priority","1");     
        $mail->addCustomHeader("X-MSmail-Priority","High");     
        $mail->addCustomHeader("X-MimeOLE", $from);
        
        if(!$mail->send()) {
            $this->error_report('Message could not be sent.->'.'Mailer Error: ' . $mail->ErrorInfo);
            return false;
        } else {
            // echo 'Message has been sent';
            // echo "ok";
            return true;
        }
    }
    
    function PHPmail($to_array, $subject, $body, $from = null, $name_from = null, $replyto = null){
        if(!$from)
        {
            $from = $this->smtp_email;
        }
        if(!$name_from)
        {
            $name_from = $this->nome_from_email;
        }
        if(!$replyto)
        {
            $replyto = $this->smtp_email;
        }
        $headers  = "From: ".$name_from." <".$from."> \n";
        $headers .= "MIME-Version: 1.0 \n";
        $headers .= "X-Priority: 1 ";
        $headers .= "X-MSmail-Priority: High \n";
        $headers .= "X-Mailer: PHP/" . phpversion()."\n";
        $headers .= "Content-type: text/html; charset=iso-8859-1 \n";
        $headers .= "X-Sender-IP: ".$this->ip_server_email." \n";
        $headers .= "Organization: ".$this->organization_email." \n";
        
        $return_flag = false;
        foreach($to_array as $key=>$value){
            $return_flag = mail($value,$subject,$body,$headers);
        }
        
        return $return_flag;
    }
}
