<?php
spl_autoload_register(function ($class) {
    include $class . '.class.php';
});

/**
 * Description of database
 *
 * @author joaopalhinha
 */
class database extends globalstuff{
    
    private $database_name;
    private $database_user;
    private $database_pass;
    private $database_host;
    
    public $tables = array();

    function __construct() {
        parent::__construct();
        
        include 'config/database.config.php';
        
        
    }
    
    /**
     * Connectar à Base de Dados
     * 
     * @return string\mysqli
     */
    function connect(){
        $mysqli_object = new mysqli($this->database_host, $this->database_user, $this->database_pass, $this->database_name);
        
        if ($mysqli_object->connect_errno) {
            $this->error_report("Failed to connect to MySQL: (" . $mysqli_object->connect_errno . ") " . $mysqli_object->connect_error);
            $result = $this->flag_error;
        }
        else{
            $result = $mysqli_object;
        }

        return $result;
    }
    
    /**
     * Query simples à base de dados
     * @param string $qry
     * @param mysqli $msqli_object
     * @return string|array
     */
    function query($qry,$msqli_object = null ) {
        $result = $this->flag_error;
        
        $flag_object = 0;
        if(!$msqli_object){
            $msqli_object = $this->connect();
            $flag_object = 1;
        }
            
        
        if($msqli_object != $this->flag_error){
            if(!$res_query = $msqli_object->query($qry)){
                $this->error_report($qry." - MySQL error: (" . $msqli_object->errno . ") " . $msqli_object->error);
                $result = $this->flag_error;
            }
            else{
                $result = array();
                while ($row = $res_query->fetch_object()){
                    $result[] = (array)$row;
                }
            }
            
            if($flag_object == 1){
                $msqli_object->close();
            }
        }
        
        
        
        return $result;
    }
    
    /**
     * Query com variaveis
     * @param string $qry
     * @param array $array_vars
     * @param string $string_var_type
     * @param mysqli $msqli_object
     * @return string|array 
     */
    function queryPrepare($qry, $array_vars, $string_var_type, $msqli_object = null) {
        $result = $this->flag_error;
        
        $flag_object = 0;
        if(!$msqli_object){
            $msqli_object = $this->connect();
            $flag_object = 1;
        }
        
        if($msqli_object != $this->flag_error){
            $stmt = $msqli_object->stmt_init();
            
            if (!($stmt->prepare($qry))) { 
                $this->error_report($qry." - Prepare failed: (" . $msqli_object->errno . ") " . $msqli_object->error);
                $result = $this->flag_error;
            }
            else{
                $a_params[] = & $string_var_type;
                $n = count($array_vars);
                for($i = 0; $i < $n; $i++) {
                    if (!$array_vars[$i] && !is_numeric($array_vars[$i])) {
                        $array_vars[$i] = null;
                    }
                    
                    $a_params[] = & $array_vars[$i];
                }
                
                call_user_func_array(array($stmt, 'bind_param'), $a_params);
                
                if (!$stmt->execute()) {
                    $this->error_report($qry." - Execute failed: (" . $stmt->errno . ") " . $stmt->error);
                    $result = $this->flag_error;
                }
                else{
                    $stmt->store_result();


                    $result = array();
                    while($assoc_array = $this->fetchAssocStatement($stmt))
                    {
                        $result[] = $assoc_array;
                    }
                    $stmt->close();

                }
            }
            
            if($flag_object == 1){
                $msqli_object->close();
            }
        }
        
        return $result;
    }
    
    /**
     * Transformar resultado de uma query num array
     * @param type $stmt
     * @return array
     */
    function fetchAssocStatement($stmt)
    {
        if($stmt->num_rows>0)
        {
            $result = array();
            $md = $stmt->result_metadata();
            $params = array();
            while($field = $md->fetch_field()) {
                $params[] = &$result[$field->name];
            }
            call_user_func_array(array($stmt, 'bind_result'), $params);
            if($stmt->fetch())
                return $result;
        }

        return null;
    }
    
    /**
     * remove o auto commit do objecto msqli
     * @param mysqli $msqlobject
     */
    function disableAutocommit($msqlobject){
        $msqlobject->autocommit(FALSE);
    }
    
    /**
     * Retorna o ultimo id criado
     * @param mysqli $msqlobject
     * @return int
     */
    function returnLastId($msqlobject){
        return $msqlobject->insert_id;
    }
    
}
