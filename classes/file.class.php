<?php
spl_autoload_register(function ($class) {
    include $class . '.class.php';
});

class file extends globalstuff{
    
    private $filetypes_array;
    function __construct() {
        parent::__construct();
        
        include("config/file.config.php");
    }
    
    function generateFileName($name){
        $array_name = explode(".", $name);
        $extensao = $array_name[count($array_name)-1];
        $stringnome = "";
        foreach ($array_name as $key => $value) {
            if($key < count($array_name)-1){
                $stringnome .= $value;
            }
        }
        $nome_final_url = $this->generateFriendlyName(date("Y-m-d H:i:s").$stringnome, 100).".".$extensao;
        return $nome_final_url;
    }
    
    function uploadFile($file,$folder,$arrayFileTypes){
        $flag = false;
        if($this->checkFileTypeBeforeUpload($file, $arrayFileTypes)){
            if($this->checkDir($folder)){
                if(move_uploaded_file($file['tmp_name'], $folder.$file['name'])){
                    $flag = $file['name'];
                }
            }
        }
        return $flag;
    }
    
    function convertExtensioToMymetype($extensionArray){
        $return_array = array();
        foreach ($extensionArray as $key => $value) {
            if(isset($this->filetypes_array[$value])){
                foreach ($this->filetypes_array[$value] as $key2 => $value2) {
                    $return_array[] = $value2;
                }
               
            }
        }
        return $return_array;
    }
            
    function checkFileTypeBeforeUpload($file,$arrayFileTypes) {
        $allowed_types = $this->convertExtensioToMymetype($arrayFileTypes);
        $fileInfo = finfo_open(FILEINFO_MIME_TYPE);
        $detected_type = finfo_file( $fileInfo, $file['tmp_name'] );
        $flag_type = true;
        if ( !in_array($detected_type, $allowed_types) ) {
            $flag_type = false;
        }
        finfo_close( $fileInfo );
        
        return $flag_type;
    }
    
    function checkDir($dir){
        $folder_array = explode("/",$dir);
        $folder_string = "";
        $flag_create = false;
        foreach ($folder_array as $key => $value) {
            $folder_string .= $value."/";
            if($value != ".."){
                if(!is_dir($folder_string)){
                    if(mkdir($folder_string, 0777)){
                        $flag_create = true;
                    }
                    else{
                        $flag_create = false;
                        break;
                    }
                }
                else{
                    $flag_create = true;
                }
            }
        }
        
        return $flag_create;
    }
}
